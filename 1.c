#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
/*
Programa que recebe dois n�meros e mostra os m�ltiplos de 3 entre ambos
*/
int main() {
	setlocale (LC_ALL, "Portuguese");
	int a,b,i, quant = 0;
	printf ("Digite A: \n");
	scanf ("%d",&a);
	printf ("Digite um n�mero maior que A: \n");
	scanf ("%d",&b);
	printf ("S�o m�ltiplos de 3: \n");
	for (i=a; i<=b; i++) {
		if (i % 3 == 0){
			printf ("%d \n", i);
			quant++;
		}
	}
	return 0;
}
